const port = 3000;
const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser');

//for logging
app.use(morgan("short"));

//for posting
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//endpoints

// http://localhost:3000/_gtg
app.use(require('./routes/_gtg.js')) 

// http://localhost:3000/generate/response
app.use('/generate',require('./routes/response.js'))

// http://localhost:3000/generate/response or http://localhost:3000/generate/response/requestParam
app.use('/generate',require('./routes/fileServer.js'))

app.listen(port, () =>{
    console.log("Server is up and listening on Port "+port+"...");
});