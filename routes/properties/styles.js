module.exports = {
    customStyles: function() {
        return {
            dataCenter: {
                fontSize: 10,
                bold: true,
                alignment: 'center',
                color: 'blue'
            }, dataLeft: {
                fontSize: 10,
                bold: true,
                alignment: 'left',
                color: 'blue'
            }, headerLeft: {
                alignment: 'left',
                fontSize: 12,
                bold: true
            }, headerCenter: {
                alignment: 'center',
                fontSize: 12,
                bold: true
            }
        }
    }
}