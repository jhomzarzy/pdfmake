var router = require('express').Router();
var PdfPrinter = require('pdfmake');

var fonts = require('./properties/fonts').Fonts()
var printer = new PdfPrinter(fonts);

var customStyles = require('./properties/styles').customStyles();

//file server
var fs = require('fs');


router.get('/fileServer', (req, res) => {
    let fileName = 'fileName';
    var docDefinition = {
        pageSize: 'A4',
        pageOrientation: 'landscape',
        watermark: { text: "watermark", color: 'gray', opacity: 0.3, bold: true },
        content: [
            {text: 'dafault style'},
            {text: 'data Center', style:'dataCenter'},
            {text: 'data Left', style:'dataLeft'},
            {text: 'header Center', style:'headerCenter'},
            {text: 'header Left', style:'headerLeft'},
            {text: 'In-Line Style', style:{fontSize:12, color:'red', alignment: 'right'}}
        ]
        , defaultStyle: {
            font: 'Courier'
        }, styles: customStyles
    };
    var pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(
        fs.createWriteStream('generatedReports/' + fileName + '.pdf').on("error", error => {
            console.log("PDF ERROR : ", error);
            res.send('500')
        }).on("finish", success => {
            res.send('200')
        })
    );
    pdfDoc.end();
});

module.exports = router;