var router = require('express').Router();
var PdfPrinter = require('pdfmake');

var fonts = require('./properties/fonts').Fonts()
var printer = new PdfPrinter(fonts);

var customStyles = require('./properties/styles').customStyles();

router.get('/response', (req, res) => {
    var docDefinition = {
        pageSize: 'A4',
        pageOrientation: 'portrait',
        watermark: { text: "watermark", color: 'gray', opacity: 0.3, bold: true },
        content: [
            {text: 'dafault style'},
            {text: 'data Center', style:'dataCenter'},
            {text: 'data Left', style:'dataLeft'},
            {text: 'header Center', style:'headerCenter'},
            {text: 'header Left', style:'headerLeft'},
            {text: 'In-Line Style', style:{fontSize:12, color:'red', alignment: 'right'}}
        ]
        , defaultStyle: {
            font: 'Courier'
        }, styles: customStyles
    };
    var pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(res);
    pdfDoc.end();
});

router.get('/response/:param', (req, res) => {
    let reqParam = req.params;
    console.log(reqParam.param);
    var docDefinition = {
        pageSize: 'A4',
        pageOrientation: 'portrait',
        watermark: { text: reqParam.param, color: 'gray', opacity: 0.3, bold: true },
        content: [
            {text: 'dafault style'},
            {text: 'data Center', style:'dataCenter'},
            {text: 'data Left', style:'dataLeft'},
            {text: 'header Center', style:'headerCenter'},
            {text: 'header Left', style:'headerLeft'},
            {text: 'In-Line Style', style:{fontSize:12, color:'red', alignment: 'right'}}
        ]
        , defaultStyle: {
            font: 'Courier'
        }, styles: customStyles
    };
    var pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(res);
    pdfDoc.end();
});

module.exports = router;